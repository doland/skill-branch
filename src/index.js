
import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2a', (req, res) => {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
});

const getInitials = (person) => {
  person = person.replace(/\s+/g, ' ').trim();

  //console.log(new Buffer(person));
  if (!person) {
    return 'Invalid fullname';
  }

  if (/[\d_/]/.test(person)) {
    return 'Invalid fullname';
  }

  const parts = person.split(' ');
  if (parts.length < 1 || parts.length > 3) {
    return 'Invalid fullname';
  }

  let initials = [parts.pop()];
  initials = [initials[0][0].toUpperCase() + initials.toString().substr(1).toLowerCase()];
  parts.forEach(function (item, i) {
    parts[i] = item[0].toUpperCase() + '.';
  });

  return initials.concat(parts).join(' ');
};

app.get('/task2b', (req, res) => {
  const result = getInitials(req.query.fullname.toString());
  res.send(result);
});

function canonize(url) {
  const re = new RegExp('(https?:)?(\/\/)?([a-zA-Z0-9\.]*[^\/]*\/)?([@])?([a-zA-Z0-9\.\_]*)(\/)?', 'i');
  const username = url.match(re);
  return username;
};

app.get('/task2c', (req, res) => {
  res.send('@' + canonize(req.query.username)[5]);
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
